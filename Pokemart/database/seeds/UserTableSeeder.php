<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('roleName', 'Admin')->first();
        $role_member = Role::where('roleName', 'Member')->first();

        $admin = new User();
        $admin->name = 'Adhitama';
        $admin->email = 'adhitamafikri@gmail.com';
        $admin->password = bcrypt('goldenchild');
        $admin->save();
        $admin->roles()->attach($role_admin);

       	$member = new User();
        $member->name = 'Bellanoche';
        $member->email = 'bellanoche@gmail.com';
        $member->password = bcrypt('bellanoche');
        $member->save();
        $member->roles()->attach($role_member);
    }
}
