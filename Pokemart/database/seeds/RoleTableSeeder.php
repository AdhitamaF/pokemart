<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->roleName = 'Admin';
        $admin->save();

        $member = new Role();
        $member->roleName = 'Member';
        $member->save();
    }
}
